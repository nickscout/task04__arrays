package com.epam.nickscout.CachedLinkedList;

import com.sun.istack.internal.NotNull;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CachedLinkedList<E> extends LinkedList<E> {

    private int size;
    private CachedLinkedListNode<E> head;
    private CachedLinkedListNode<E> tail;
//    private LinkedHashMap<Integer, CachedLinkedListNode<E>> cache;
    public CacheMap<E> cache;

    private int cacheSize;

    public CachedLinkedList() {
        size = 0;
        head = new CachedLinkedListNode<>(null);
        tail = head;
        cacheSize = 32;
        //Fifo hash map
        cache = new CacheMap<>(cacheSize);
    }

    public CachedLinkedList(Collection<? extends E> collection) {
        size = 0;
        head = new CachedLinkedListNode<>(null);
        tail = head;
        cacheSize = 32;
        //Fifo hash map
        cache = new CacheMap<>(cacheSize);
        this.addAll(collection);
    }

    public CachedLinkedList(int cacheSize) {
        size = 0;
        head = new CachedLinkedListNode<>(null);
        tail = head;
        this.cacheSize = cacheSize;
        //Fifo hash map
        cache = new CacheMap<>(cacheSize);
    }

    public CachedLinkedList(Collection<? extends E> collection, int cacheSize) {
        size = 0;
        head = new CachedLinkedListNode<>(null);
        tail = head;
        this.cacheSize = cacheSize;
        //Fifo hash map
        cache = new CacheMap<>(cacheSize);
        this.addAll(collection);
    }

    public E get(int i) {
        return getNode(i).getElement();
    }

    protected CachedLinkedListNode<E> getNode(int i) {
        if (i - 1 > size) throw new ArrayIndexOutOfBoundsException();
        if (cache.containsKey(i)) {
            return cache.get(i);
        } else if (i < size/2) {
            CachedLinkedListNode<E> node = tail;
            for (int j = 0; j != i; j++) {
                node = node.getNext();
            }
            cache.put(i, node);
            return node;
        } else {
            CachedLinkedListNode<E> node = head;
            for (int j = size - 1; j != i; j--) {
                node = node.getPrev();
            }
            cache.put(i, node);
            return node;
        }
    }

    public void forEach(Consumer<? super E> consumer) {
        CachedLinkedListNode<E> node = tail;
        for (int i = 0; i < size; i++) {
           consumer.accept(node.getElement());
           node = node.getNext();
        }
    }

    @Override
    public E getFirst() {
        return tail.getElement();
    }

    @Override
    public E getLast() {
        return head.getElement();
    }

    @Override
    public E removeFirst() {
        E e = tail.getElement();
        remove(0);
        return e;
    }

    @Override
    public E removeLast() {
        E e = tail.getElement();
        remove(size - 1);
        return e;
    }

    @Override
    public void addFirst(E e) {
        if (size == 0) {
            tail.setElement(e);
        } else if (size == 1) {
            tail = new CachedLinkedListNode<>(e);
            tail.setNext(head);
            head.setPrev(tail);
        } else {
            CachedLinkedListNode<E> node = new CachedLinkedListNode<>(e);
            tail.setPrev(node);
            node.setNext(tail);
            tail = node;
        }
        size++;
    }

    @Override
    public void addLast(E e) {
        if (size == 0) {
            head.setElement(e);
        }
        else if (size == 1) {
            head = new CachedLinkedListNode<>(e);
            head.setPrev(tail);
            tail.setNext(head);
        } else {
            CachedLinkedListNode<E> node = new CachedLinkedListNode<>(e);
            head.setNext(node);
            node.setPrev(head);
            head = node;
        }
        size++;
    }

    @Override
    public boolean add(E e) {
        addLast(e);
        return true;
    }

    @Override
    public void add(int i, E e) {
        addNoTrim(i, e);
        cache.trim(i);
    }

    public void addNoTrim(int i, E e) {
        CachedLinkedListNode<E> ithNode = getNode(i);
        CachedLinkedListNode<E> prevNode = ithNode.getPrev();
        CachedLinkedListNode<E> node = new CachedLinkedListNode<>(e);

        CachedLinkedListNode.tieUpNodes(node, ithNode);
        CachedLinkedListNode.tieUpNodes(prevNode, node);

        size++;
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        collection.forEach(this::add);
        return true;
    }

    @Override
    public boolean addAll(int i, Collection<? extends E> collection) {
        Iterator<? extends E> iterator = collection.iterator();
        for (int j = 0; j < collection.size(); i++, j++) {
            this.addNoTrim(i, iterator.next());
        }
        cache.trim(i);
        return true;
    }

    @Override
    public boolean contains(Object o) {
        return this.stream().anyMatch(e -> e.equals(o));
    }

    @Override
    public Stream<E> stream() {
        Stream.Builder<E> builder = Stream.builder();
        this.forEach(builder::add);
        return builder.build();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean remove(Object o) {
        return removeByObj((E) o);
    }

    public boolean removeByObj(E e) {
        CachedLinkedListNode<E> node = tail;
        while (node.hasNext()) {
            if (node.getElement().equals(e)) {
                CachedLinkedListNode<E> prev = node.getPrev();
                CachedLinkedListNode<E> next = node.getNext();
                CachedLinkedListNode.tieUpNodes(prev, next);
                cache.removeByValue(node.getElement());
                size--;
            }
            node = node.getNext();
        } return true;
    }

    @Override
    public void clear() {
        head = null;
        tail = null;
        cache.clear();
    }

    @Override
    public E set(int i, E e) {
        CachedLinkedListNode<E> node = getNode(i);
        E prevE = node.getElement();
        node.setElement(e);
        return prevE;
    }

    @Override
    public E remove(int i) {

        if (i == size - 1) {
            E e = head.getElement();
            head = head.getPrev();
            head.setNext(null);
            cache.removeByValue(e);
            size--;
            return e;
        } else if (i == 0) {
            E e = tail.getElement();
            tail = tail.getNext();
            tail.setPrev(null);
            cache.removeByValue(e);
            size--;
            return e;
        } else {
            CachedLinkedListNode<E> node = getNode(i);
            CachedLinkedListNode<E> prev = node.getPrev();
            CachedLinkedListNode<E> next = node.getNext();

            E e = node.getElement();

            CachedLinkedListNode.tieUpNodes(prev, next);
            if (cache.containsValue(e)) {
                cache.removeByValue(e);
            }
            node = null;
            size--;
            return e;
        }
    }

    @Override
    public int indexOf(Object o) {
        if (cache.containsValue(o)) {
            return cache.keySet()
                    .stream()
                    .filter(key -> cache.get(key).getElement().equals(o))
                    .mapToInt(key -> key)
                    .findFirst()
                    .getAsInt();
        }
        else {
            int i = 0;
            CachedLinkedListNode<E> node = tail;
            while (node.hasNext()) {
                if (node.getElement().equals(o)) {
                    return i;
                } else {
                    i++;
                    node = node.getNext();
                }
            }
            return -1;
        }
    }

    @Override
    public int lastIndexOf(Object o) {
        if (cache.containsValue(o)) {
            IntStream stream = cache.keySet()
                    .stream()
                    .filter(key -> cache.get(key).getElement().equals(o))
                    .mapToInt(key -> key);
            long count = stream.count();
            return stream.skip(count - 1).findFirst().getAsInt();


        }
        else {
            int i = size - 1;
            CachedLinkedListNode<E> node = head;
            while (node.hasPrev()) {
                if (node.getElement().equals(o)) {
                    return i;
                } else {
                    i--;
                    node = node.getPrev();
                }
            }
            return -1;
        }
    }

    @Override
    public E peek() {
        return head.getElement();
    }

    @Override
    public E element() {
        return peek();
    }

    @Override
    public E poll() {
        E e = head.getElement();
        remove(head);
        return e;
    }

    @Override
    public E remove() {
        return poll();
    }

    @Override
    public boolean offer(E e) {
        addFirst(e);
        return true;
    }

    @Override
    public boolean offerFirst(E e) {
        addLast(e);
        return true;
    }

    @Override
    public boolean offerLast(E e) {
        addFirst(e);
        return true;
    }

    @Override
    public E peekFirst() {
        if (head == null) { return null; }
        else { return head.getElement(); }
    }

    @Override
    public E peekLast() {
        if (tail == null) { return null; }
        else { return tail.getElement(); }
    }

    @Override
    public E pollFirst() {
        return removeFirst();
    }

    @Override
    public E pollLast() {
        return removeLast();
    }

    @Override
    public void push(E e) {
        add(e);
    }

    @Override
    public E pop() {
        return head.getElement();
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        CachedLinkedListNode<E> node = tail;
        while (node.hasNext()) {
            if (node.getElement().equals(o)) {
                CachedLinkedListNode<E> prev = node.getPrev();
                CachedLinkedListNode<E> next = node.getNext();
                CachedLinkedListNode.tieUpNodes(prev, next);
                cache.removeByValue(node.getElement());
                size--;
                return true;
            }
            node = node.getPrev();
        } return true;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        CachedLinkedListNode<E> node = head;
        while (node.hasPrev()) {
            if (node.getElement().equals(o)) {
                CachedLinkedListNode<E> prev = node.getPrev();
                CachedLinkedListNode<E> next = node.getNext();
                CachedLinkedListNode.tieUpNodes(prev, next);
                cache.removeByValue(node.getElement());
                size--;
                return true;
            }
            node = node.getPrev();
        } return true;
    }

    @Override
    public Object clone() {
        CachedLinkedList<E> cloneList = new CachedLinkedList<>();
        forEach(e -> cloneList.add(e));
        return cloneList;
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[size];
        CachedLinkedListNode<E> node = tail;
        int i = 0;
        while (node.hasNext()) {
            array[i++] = node.getElement();
            node = node.getPrev();
        }
        return array;
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        CachedLinkedListNode<E> node = tail;
        int i = 0;
        while (node.hasNext()) {
            ts[i++] = (T) node.getElement();
            node = node.getPrev();
        }
        return ts;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Cached Linked List = { ");
        forEach(e -> {
            sb.append(e.toString()).append(" ");
        });
        sb.append("}");
        return sb.toString();
    }
}

class CachedLinkedListNode<E> {
    private CachedLinkedListNode<E> prev;
    private CachedLinkedListNode<E> next;
    private E element;

    public static <E> void tieUpNodes(@NotNull CachedLinkedListNode<E> prevNode, @NotNull CachedLinkedListNode<E> nextNode) {
        prevNode.setNext(nextNode);
        nextNode.setPrev(prevNode);
    }

    public CachedLinkedListNode(E element) {
        this.element = element;
    }

    public boolean hasNext() { return next != null; }

    public boolean hasPrev() { return prev != null; }

    public CachedLinkedListNode<E> getPrev() {
        return prev;
    }

    public void setPrev(CachedLinkedListNode<E> prev) {
        this.prev = prev;
    }

    public CachedLinkedListNode<E> getNext() {
        return next;
    }

    public void setNext(CachedLinkedListNode<E> next) {
        this.next = next;
    }

    public E getElement() {
        return element;
    }

    public void setElement(E element) {
        this.element = element;
    }

    @Override
    public String toString() {
        return element.toString();
    }
}

class CacheMap<V> extends LinkedHashMap<Integer,CachedLinkedListNode<V>> {

    private final int cacheSize;

    public CacheMap(int cacheSize) {
        this.cacheSize = cacheSize;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<Integer,CachedLinkedListNode<V>> entry) {
        return size() > cacheSize;
    }

    public void trim (int thershhold) {
        Set keys = keySet().stream().filter(key -> key >= thershhold).collect(Collectors.toSet());
        for (Object key : keys) {
            remove(key);
        }
    }

    public void removeByValue(V v) {
        Set keys = keySet().stream().filter(key -> get(key).equals(v)).collect(Collectors.toSet());
        for (Object key : keys) {
            remove(key);
        }
    }
}