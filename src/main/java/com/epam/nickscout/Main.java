package com.epam.nickscout;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.LinkedList;



public class Main {
    public static final Logger LOG = LogManager.getLogger(Main.class.getName());
    public static void main(String[] args) {
        logicTaskB();
    }

    public static void logicTaskA() {
        int[] arr1 = {3,5,9};
        int[] arr2 = {7,1,5};

        int[] arrResultBoth = new int[arr1.length];
        int[] arrResultSingle = new int[arr1.length*2];

        int i = 0;
        int j = 0;
        for (int i1 = 0; i1 < arr1.length; i1++) {
            for (int i2 = 0; i2 < arr2.length; i2++) {
                if (arr1[i1] == arr2[i2]) arrResultBoth[i++] = arr1[i1];
            }
        }

        int k = 0;
        for (int i1 = 0; i1 < arr1.length; i1++) {
            for (int iR = 0; iR < i; iR++) {
                if (arr1[i1] != arrResultBoth[iR]) {
                    arrResultSingle[k++] = arr1[i1];
                }
            }
        }

        for (int i1 = 0; i1 < arr2.length; i1++) {
            for (int iR = 0; iR < i; iR++) {
                if (arr2[i1] != arrResultBoth[iR]) {
                    arrResultSingle[k++] = arr2[i1];
                }
            }
        }

        LOG.debug(Arrays.toString(arrResultBoth));
        LOG.debug(Arrays.toString(arrResultSingle));

    }

    public static void logicTaskB() {
        
    }
}
